\documentclass[10pt,compress]{beamer}
\usepackage{appendixnumberbeamer}
\usepackage[utf8]{inputenc}
\usetheme{metropolis}
\usepackage{amsmath}
\usepackage[mathrm=sym,bold-style=ISO]{unicode-math}

\definecolor{darkgrey}{RGB}{49,68,85}
\definecolor{darkred}{RGB}{57,3,1}

\graphicspath{{./images/}}
\usepackage{tikz}
\usetikzlibrary{calc}
\newcommand{\hexcoord}[2]
        {[shift=(0:#1),shift=(60:#1),shift=(0:#2),shift=(-60:#2)]}

\usepackage[backend=biber,style=numeric,sorting=none,doi=false,isbn=false,maxbibnames=4,firstinits=true,url=false]{biblatex}
\AtEveryBibitem{\clearfield{pages}}
\AtEveryBibitem{\clearfield{month}}
\AtEveryBibitem{\clearfield{title}}
\addbibresource{biblio.bib}

\DeclareMathOperator{\gap}{gap}
\DeclareMathOperator{\id}{id}
\newcommand{\identity}{\mathds{1}}
\usepackage{physics}
\usepackage{dsfont}
% \usepackage{bm}
\newcommand{\bm}{\mathbf}

\newcommand{\compresslist}{%
  \setlength{\itemsep}{1pt}%
  \setlength{\parskip}{0pt}%
  \setlength{\parsep}{0pt}%
}

\newcommand{\sym}{\mathrm{sym}}
\newcommand{\traop}{\mathds{E}}

\title{Spectral gap for AKLT models on arbitrary decorated graphs}

\author[A. Lucia]{Angelo Lucia (Universidad Complutense de Madrid \& ICMAT)}
\institute{ Joint work with A. Young \\
  Based on J. Math. Phys. 64, 041902 (2023) \href{https://arxiv.org/abs/2212.11872}{arXiv:2212.11872 [math-ph]}.}
\titlegraphic{
  \center
  \includegraphics[height=15pt]{FSE.png}
  \hspace{1.5cm}
  \includegraphics[height=15pt]{MICINN.png}
  \hspace{1.5cm}
  \includegraphics[height=15pt]{UCM.png}
}
\date{Quantum and Dynamical 2023 Christmas Workshop, Milan, 21/12/2023}

\begin{document}
\begin{frame}
\maketitle
\end{frame}

\begin{frame}{AKLT models}

  AKLT \footfullcite{AKLT87, AKLT88} introduced a family of isotropic   ($SU(2)$-invariant) spin models: for an infinite graph $G$

  \begin{itemize}
    \item at a each vertex $v$, a spin-$\deg(v)/2$ particle, i.e., $\mathds{C}^{\deg(v)+1}$
    \item nearest-neighbor interaction: for each edge $e = (u,v)$, let $P_{e}^{z(e)/2}$ be the orthogonal projection on the subspace of maximal spin-$z(e)/2$, where $z(e) = \deg(v)+\deg(w)$
    \item $H_{\Lambda} = \sum_{e \in \Lambda} P_{e}^{{z(e)}/2}$ for each finite $\Lambda \subset G$.
    \end{itemize}

  \end{frame}

  \begin{frame}{AKT models}

    \begin{block}{Haldane ``conjecture''}
      Heisenberg antifferromagnetic chain: $\sum_{i} \bm{S}_i \cdot \bm{S}_{i+1}, \quad \bm{S} = (S^{X}, S^{Y}, S^{Z})$

      \begin{enumerate}
      \item for half-integer spin models: ``massless'' phase (gapless)
      \item for integer spin models: ``massive'' phase (unique ground state, exp. decay of correlations, spectral gap).
      \end{enumerate}
      \textbf{Spectral gap:} difference between the two (distinct) smallest energy levels.
    \end{block}

    What AKLT original and subsequent papers\footfullcite{KLT88} show:
    \begin{itemize}
    \item 1D spin-1 chain belongs to the \emph{Haldane phase}.
    \item unique ground state (with periodic b.c.) on any graph.
    \item spin-3/2 hexagonal chain has exponential decay of correlations.
    \end{itemize}

    \vspace{1em}
  \end{frame}

  \begin{frame}[standout]
    \textbf{Open problem:}

    For which graphs does the AKLT model have a spectral gap?
  \end{frame}
  

  \begin{frame}{Edge-decorated AKLT models}

    \begin{center}
      \begin{tikzpicture}[scale=0.9]
        % Five-by-five hexagonal grid
        \foreach \x in {0,...,1}
        \foreach \y in {0,1}{
          \draw\hexcoord{\x}{\y}
          (0:1)--(60:1)--(120:1)--(180:1)--(-120:1)--(-60:1)--cycle;
          \foreach \n in {0, 60, 120, 180, -120, -60}{
            \draw\hexcoord{\x}{\y} (\n:1)--(\n:1.3);
            \draw[fill=black]\hexcoord{\x}{\y} (\n:1) circle (2.5pt);
            \draw[fill=black]\hexcoord{\x}{\y} (\n+20:{sqrt((2*cos(\n)+cos(\n+60))^2/9+(2*sin(\n)+sin(\n+60))^2/9)}) circle (1.2pt);
            \draw[fill=black]\hexcoord{\x}{\y} (\n+40:{sqrt((cos(\n)+2*cos(\n+60))^2/9+(sin(\n)+2*sin(\n+60))^2/9)}) circle (1.2pt);
          }
        }
        \node (a) at (-1.5,0.5) {$3/2$};
        \node (b) at (-1.05,0.05) {};
        \draw[darkred] (a) edge[bend right=10,->] node[left] {} (b);
        \node (c) at (0, 1.5) {$1$};
        \node (d) at (-0.15, 0.9) {};
        \node (e) at (0.15, 0.9) {};
        \draw[darkred] (c.south west) edge[bend right=5,->] node[left] {} (d);
        \draw[darkred] (c.south east) edge[bend left=5,->] node[left] {} (e);
      \end{tikzpicture}
    \end{center}
  
    In \footfullcite{1901.09297}, we looked at a \alert{decorated} hexagonal lattice: replace each edge in $G$ with a 1D chain of length $n$
  \begin{theorem}
    The model on the decorated hexagonal lattice is gapped when $n\ge 3$.
  \end{theorem}
\end{frame}

\begin{frame}{Subsequent results}
  \vspace{-1em}
  \begin{columns}
    \begin{column}[t]{0.3\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{pomata-1.png}
        \caption{\fullcite{Pomata2019}}
      \end{figure}
    \end{column}
    \begin{column}[t]{0.3\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{pomata-2.png}
        \caption{\fullcite{2010.03137}}
      \end{figure}
    \end{column}
    \begin{column}[t]{0.3\textwidth}
      \begin{figure}
        \includegraphics[width=\textwidth]{pomata-3.png}
        \caption{\fullcite{2103.11819}}
      \end{figure}
    \end{column}       
  \end{columns}

  Some ideas ended up in one of the numerical-assisted proofs of the gap for the hexagonal model \\ {\footnotesize \fullcite{Pomata2020}}.
  
\end{frame}

\begin{frame}{A general result}

  \begin{theorem}[L., Young, {\footnotesize \href{https://arxiv.org/abs/2212.11872}{arXiv:2212.11872 [math-ph]}}]
    Let $G$ be a simple graph of maximal degree $\Delta(G)$, with $3 \le
    \Delta(G)< \infty$. Consider the $n$-decoration $G^{(n)}$ of $G$, with $n \ge n(\Delta(G))$, where $n(d)$ is a linear function of $d$. Then
    \[ \gap(H_{G^{(n)}}) \ge \gamma(\Delta(G),n) > 0  \]
    so that the AKLT model on the decorated graph is gapped.
  \end{theorem}


  The proof is based on a \alert{Transfer Operator}/\alert{Tensor networks} approach.
\end{frame}



\begin{frame}{Step 1: Coarse-graining}

  \begin{exampleblock}{Step 1}
    Consider
    \[
      \tilde H = \sum_{v \in G} P_v \qc P_v = 1-G_v
    \]
    where $G_v$ is orth. proj. onto ground state subspace of $H_{Y_v}$.
    \begin{center}
      \begin{figure}
        \includegraphics[width=\textwidth]{decorated_graph.png}
      \end{figure}
      $Y_v$ inclues $v$ and all the \emph{decorations} around it.
    \end{center}

    Then $\gap(H_{G^{(n)}}) \ge \frac{1}{2} \gap(H_{Y_v}) \cdot \gap(\tilde H)$.
  \end{exampleblock}
\end{frame}

\begin{frame}{Step 2: Squaring}

  \begin{exampleblock}{Step 2}
    \[
      \tilde H^2 = \sum_{u,v \in G} P_u P_v \ge  \tilde H - \epsilon_G(n) \sum_{ \substack{ (u,v) \in G  }} \qty(P_v + P_w)
    \]
    where
    \[
      \epsilon_G(n) := \sup_{(u,v)\in G} \norm{G_u G_v - G_u \wedge G_v}
    \]
    \pause
    Therefore
    \[
       \gap(\tilde H) \ge 1 - \Delta(G) \epsilon_G(n).
    \]  
  \end{exampleblock}

  \begin{alertblock}{Our objective}
    Show for which values of $n$ it holds that $\Delta(G) \epsilon_G(n)< 1$.
  \end{alertblock}
  
\end{frame}

\begin{frame}{Step 3: Transfer operator}

  \begin{center}
    \begin{figure}
        \includegraphics[width=\textwidth]{tensor_vertex.png}
    \end{figure}
  \end{center}

  Using a Matrix Product State representation for the ground states, we can define \alert{transfer operators}:
  \[
    \traop_L^{(d_L,n)} = (\traop^{\otimes {(d_L-1)}})^{n} \circ \traop_L^{(d_L,0)}
  \]
  and similarly $\traop_R^{(d_R,n)} =  \qty(\traop_L^{(d_R,n)})^*$, where $\traop$ is the 1D AKLT chain transfer operator:
    \[
      \traop = \dyad{\identity}{\identity/2} -\frac{1}{3}\sum_{U=X,Y,Z} \dyad*{\sigma^{U}}{S^{U}}
    \]
    $\sigma^{U}$ are Pauli, $S^{U} = \sigma^{U}/2$ are spin matrices.  
  \end{frame}

  \begin{frame}{Step 3: Transfer operator}

    \begin{block}{Lemma}
      Let $q_L^{(d,n)}$ the minimal eigenvalue of $\traop_L^{(d,n)}(\identity)$, and
      \[ b(d,n) := \frac{4}{3^{n}} \frac{
        \norm*{\traop_L^{(d,n)}}_\infty}{q_L^{(d,n)}} \]
       Then $\epsilon_G(n)$ is bounded by
       \[
          \frac{4}{3^{n}} \sup_{(v_L,v_R)\in G} F( b(d_L,n), b(d_R,n) ),
       \]
       for $F(\cdot, \cdot)$ a (relatively simple) explicit function (diverging at $F(1,1)=\infty$).
     \end{block}
     \pause
     \begin{alertblock}{Our objective - v2:}
       Obtain bounds on $\norm*{\traop_L^{(d,n)}}_\infty$ and $\traop_L^{(d,n)}(\identity)$
   \end{alertblock}
     
  \end{frame}
  
  \begin{frame}{The technical difficulty in the proof}
    \begin{alertblock}{Our objective - v2:}
      Obtain bounds on $\norm*{\traop_L^{(d,n)}}_\infty$ and $\traop_L^{(d,n)}(\identity)$
    \end{alertblock}
     
  The hard part of the proof boils down to the following related problems:

  \begin{exampleblock}{Problem}
    Estimate, as a function of $m=d-1$ and $n$,
    \begin{enumerate}
    \item Hilbert-Schmidt norm of $\id \otimes (\traop^{\otimes m-1})^{n} (P_{\sym}^{(m)})$, \\ from which we obtain a SVD decomposition of $\traop_L^{(d,n)}$.
    \item smallest eigenvalue of $\traop_L^{(d,n)}(\identity) =(\traop^{\otimes m})^{n}(P_{\sym}^{(m)})$;
    \end{enumerate}   
  \end{exampleblock}

  $P^{{(m)}}_{\sym}$ is the projection on the \textbf{symmetric subspace} of $(\mathds{C}^{2})^{\otimes m}$.

\end{frame}

\begin{frame}[standout]
  Matchings operators
\end{frame}

\begin{frame}{Casimir operator}


  \begin{block}{Casimir operator}
    \[ \mathcal{B}( (\mathds{C}^{2})^{\otimes m}) \ni C^{(m)} = \qty(\sum_{i=1}^{m} \bm{S}_{i})^{2} = \sum_{j \in J_{m}} j(j+1) Q^{(m)}_{j} \]

  \end{block}
  where $J_{m}$ are the possible spin values,
  \[ J_{m} = \left\{ j_{0} + k \mid 0 \le k \le \left\lfloor\frac{m}{2}\right\rfloor\right\} \qc j_{0} = \begin{cases}
                                                                                                0 & \text{$m$ is even} \\
                                                                                                \frac{1}{2} & \text{$m$ is odd}
                                                                                              \end{cases}
                                                                                            \]
  $\abs{J_{m}} = \lfloor\frac{m}{2}\rfloor +1=s+1$.

  Let $\mathcal{Z}^{(m)}$ be the commutative algebra generated by $C^{(m)}$
  \[
    \mathcal{Z}^{(m)} = \operatorname{span}\{ Q^{(m)}_{j} \mid j
    \in J_{m}\} = \{ p(C^{(m)}) \mid p \in \mathds{C}[t] \}.
  \]

  \textbf{Note:} $P_{\sym}^{(m)}$ is $Q^{(m)}_{j}$ for $j=\max J_{m} = j_{0} +s$.

\end{frame}

\begin{frame}{Matchings}

  \begin{block}{$r$-matching}
    For $1\le r \le s$, a \emph{$r$-matching} of $[m] = \{1,\dots, m\}$ is a collection of $r$ disjoint (unordered) pairs
    \[
      p = \{ (a_1,b_1),\dots, (a_{r},b_{r})\} \qc a_{i}, b_{i} \in [m]
    \]
    Let $\mathcal{M}_{r}^{m}$ be the collection of all $r$-matchings of $[m]$, and $\mathcal{M}_{0}^{m}$ contains only the \emph{empty matching} $p=\{ \}$.
  \end{block}

  We can count them:
  \[
    \abs{\mathcal{M}_{r}^{(m)}} = \frac{1}{r!} \binom{m}{2,\dots, 2, m-2r} = \binom{m}{2r}(2r-1)!!
  \]

\end{frame}

\begin{frame}{Matchings operators}

  For $1 \le r \le$, the \emph{$r$-matching operator} $M_{r}^{(m)} \in \mathcal{Z}^{(m)}$ is
  \[
    M_{r}^{(m)} = \sum_{p \in \mathcal{M}_{r}^{m}} S_{p} \qc S_{p} = \prod_{(a,b) \in p} \bm{S}_{a} \cdot \bm{S}_{b},
  \]
  while we define $M_{0}^{(m)} = \identity$.

  \pause

  \begin{enumerate}[<+->]
    \item $M_{r}^{(m)} \in \mathcal{Z}^{(m)}$ due to $SU(2)$ and permutations invariance;
    \item $M_{r}^{(m)} \neq 0$, since $S_{p} \ket{\uparrow \dots \uparrow} = \frac{1}{4^{r}}\ket{\uparrow \dots \uparrow}$;
    \item $\traop^{\otimes m}(M_{r}^{(m)}) = 3^{-2r} M_{r}^{(m)}$ -- \alert<.>{good!};
    \item $C^{(m)} = \frac{3m}{4} \identity + 2M_{1}^{(m)}$. Therefore, $M_1^{(m)}$ also generates $\mathcal{Z}^{(m)}$.
  \end{enumerate}

\pause[\thebeamerpauses]

\begin{block}{Proposition}
  The set $\{ M_{r}^{(m)}\}_{r=0}^{s}$ is a Hilbert-Schmidt orthogonal basis of $\mathcal{Z}^{(m)}$, with
  \[ \norm*{M_{r}^{(m)}}_{2}^{2} = \qty( \frac{(2r-1)!!}{4^{r}} )^{2} (2r+1) \binom{m}{2r} 2^{m}  \]
\end{block}
\end{frame}

\begin{frame}{Decomposing $P_{\sym}$}

  \begin{exampleblock}{Problem - v2}
  We want to find coefficients $p^{(m)}_r$ such that
  \[
    P_{\sym}^{(m)} = \sum_{r=0}^{s} p^{(m)}_r M_{r}^{(m)} \uncover<2->{= \bm{p}^{(m)} \cdot \bm{M}^{(m)} \qc \bm{M}^{(m)} = (M_0^{(m)}, \dots, M_s^{(m)})}
  \]
  \end{exampleblock}

  \pause[3]

  From there we should be able to solve Problem - v1
  \begin{itemize}
  \item $ (\traop^{\otimes m})^n(P^{(m)}_{\sym}) = \sum_{r=0}^{s} 3^{-2rn} p^{(m)}_r M_r^{(m)} $
  \item similar calculation allows to estimate HS norm of $\id \otimes (\traop^{\otimes m-1})^{n} (P_{\sym}^{(m)})$
  \end{itemize}
  
\end{frame}
\begin{frame}{Recursive relation}
  \begin{lemma}
    For $0 \le r \le s$ we have that
    \[
      M_{1}^{(m)}M_{r}^{(m)} = c_{r} M_{r-1}^{(m)} + a_{r} M_{r}^{(m)} + b_{r} M_{r+1}^{(m+1)}
    \]
    for
    \[
      a_{r} = \frac{r}{2}(m-2r-1) \qc b_{r} = r+1 \qc c_{r} = \frac{2r+1}{16}\binom{m-2r+2}{2}.
    \]
    Convention: $M_{-1}^{(m)} = M_{s+1}^{(m)} = 0$.
  \end{lemma}

  Proof is based the ``multiplication rules'' of spin matrices (plus a bit of counting):
  \[
    \qty( \bm{S}_a \cdot \bm{S}_b)^2 = \frac{3}{16}\identity -\frac{1}{2}\bm{S}_a \cdot \bm{S}_b \qc
    \acomm{\bm{S}_a \cdot \bm{S}_b}{\bm{S}_b \cdot \bm{S}_c} = \frac{1}{2} \bm{S}_a \cdot \bm{S}_c.
  \]

\end{frame}

\begin{frame}{Recursive relation - a corollary}

  Let $B \in \mathcal{B}(\mathds{C}^{s+1})$ the \emph{tridiagonal} matrix
  \[
    B = \mqty( a_{0} & c_{1} & 0 & \dots & 0 \\
    b_{0} & a_{1} & c_{2} & \ddots & 0 \\
    0 & \ddots & \ddots & \ddots & \vdots \\
    \vdots & \ddots & \ddots & \ddots & c_{s} \\
    0 & \dots & \dots & b_{s-1} & a_{s}
    )
  \]
  \only<1>{
  Let $\bm{e}_{0} = (1,0,\dots,0) \in \mathds{C}^{s+1}$ so that $\bm{e}_0 \cdot \bm{M}^{(m)} = \identity$.
    
  Then for any polynomial $q \in \mathds{C}[t]$, we have that
  \[
    q(M_{1}^{(m)}) = \bm{x} \cdot \bm{M}^{(m)} = \sum_{r=0}^s x_r M_r^{(m)} \qc \text{where }  \bm{x} = q(B)\bm{e}_{0}.
  \]}

\only<2>{
    By choosing $q$ appropiately
  \[
    P_{\sym}^{(m)} = q(M_{1}^{(m)})  \Rightarrow P_{\sym}^{(m)} = \bm{p}^{(m)}\cdot \bm{M}^{(m)} \quad \text{for}\quad \bm{p}^{m} = q(B)\bm{e}_{0}
  \]

  \begin{exampleblock}{Problem - v3}
  Compute $q(B)\bm{e}_{0}$ for such $q(t)$.
  \end{exampleblock}

  }

\end{frame}

\begin{frame}{Spectral theory of $B$}

  \begin{block}{Lemma}
    The spectrum of $B$ is the same as the spectrum of $M_{1}^{(m)}$ (up to multiplicities).

    Moreover, if $\ket{w} \in (\mathds{C}^{2})^{\otimes m}$ is an eigenvector of $M_{1}^{(m)}$ with eigenvalue $\lambda$, then $\bm{y} \in \mathds{C}^{s+1}$ given by
    \[
      y_r = \expval{M_{r}^{(m)}}{w} \qc r = 0, \dots, s
    \]
    is a left eigenvector of $B$ with same eigenvalue.
  \end{block}
  \pause
  Since $B$ is \alert<2>{tridiagonal}, it is similar to a symmetric matrix, so if we define $D = \operatorname{diag}(d_{0}, \dots, d_{s})$ with
  \[
    d_{0} = 1 \qc d_{r} = \frac{b_{r-1}}{c_{r}} d_{r-1} \qc r=1, \dots, s
  \]
  then \uncover<3>{for every $q(t) \in \mathds{C}[t]$}
  \[
    \only<3>{q(B)} \only<2>{B} = \sum_{k=0}^{s} \only<3>{q(}\lambda_{k}\only<3>{)} \frac{1}{\expval{D}{\bm{y}_{k}}} D\dyad{\bm{y}_{k}}
  \]
\end{frame}

\begin{frame}{Obtaining the decomposition}

  \begin{theorem}
    \[ P_{\sym}^{(m)} = \sum_{r=0}^{s} p^{(m)}_r M_{r}^{(m)} \qc p^{(m)}_r = \frac{m+1}{2} \frac{4^{r}}{(2r+1)!!} \]
  \end{theorem}
\pause
  \textbf{Proof:}   \[
  \bm{p}^{(m)} =  q(B)\bm{e}_{0} = \frac{y_{s}(0)}{\expval{D}{\bm{y}_{s}}} D\ket{\bm{y}_{s}}.
  \]
  We can compute $\bm{y}_{s}$ since $\ket{\uparrow, \dots, \uparrow}$ is an eigenvector of $M_{1}^{(m)}$ for $\lambda_{s}$.

\end{frame}

\begin{frame}{Some open questions regarding matching operators}

  \begin{enumerate}[<+->]
  \item We have two different orthogonal bases for $\mathcal{Z}^{(m)}$: \\ $\{Q^{(m)}_{j}\}_{j \in J_{m}}$ and $\{ M_{r}^{(m)}\}_{r=0}^{s}$. Change of basis matrices?
    \begin{itemize}[<+->]
    \item $ Q^{(m)}_{j} = \sum_{r} c^{(m)}(j, r) M_{r}^{(m)}$ 
         
          Compute $\expval{M_r^{(m)}}{\omega_j}$ for $\ket{\omega_j}$ an eigenvector of $M_{1}^{(m)}$ (or $C^{(m)}$) for all distinct eigenvalues.
    \item $
          M_{r}^{(m)} = \sum_{j} z^{(m)}(r,j) Q^{(m)}_{j}$
          $\Rightarrow z^{(m)}(r, \cdot)$ are eigenvalues of $M_{r}^{(m)}$.
          Recursion relation for $z^{(m)}(r,j)$. Closed formulas?
        \item Note: we know $z^{(m)}(0,j)$, $z^{(m)}(1,j)$ and
          \[
          z^{(m)}(r,j_{0}) = (-1)^{r} \binom{ \lfloor\frac{m}{2}\rfloor }{r} \frac{(2r+1)!!}{4^{r}}.
          \]      
        \end{itemize}
      \item Generalizations to $(\mathds{C}^{d})^{\otimes m}$?
      \end{enumerate}
      
\end{frame}


\begin{frame}{Outlook}

  \begin{itemize}[<+->]
  \item For decoration $n \ge n(\Delta(G))$, the AKLT model on $G^{(n)}$ is gapped.
  \item For the case $\Delta(G) = 3,4$ we recover previous analytical results. Numerical calculations show better bounds on $n(d)$.
  \item Bounds for edge-dependent decoration number $n$ can be obtained using the same tools.
  \item $n(d)$ is \emph{linear}. Is this optimal?
  \item AKLT models on undecorated lattices with large coordination number are expected to have Néel order. What is the minimal $n$ making them gapped?
  \item When are these decorated models in the same phase as the undecorated ones?
  \end{itemize}
  
  \pause[\thebeamerpauses]

  \begin{centering}
    \textbf{Thanks for your attention!}
  \end{centering}

\end{frame}

\end{document}
